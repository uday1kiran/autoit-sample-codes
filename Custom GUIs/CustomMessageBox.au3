#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Array.au3>

if UBound($CmdLine)>1 then
	;_ArrayDisplay($CmdLine)
	;MsgBox(0,"",UBound($CmdLine))
	$t = 10
	if UBound($CmdLine)=4 then $t = $CmdLine[3]
	MyMessage($CmdLine[1],$CmdLine[2],$t)
Else
	MyMessage("System reboot requested..still it is restart yes","System Reboot requested,Dow IT has made changes to your system that require a reboot. Please restart your system at a time that is convenient to you within the next 24 hours")
EndIf


Func MyMessage($headingtext="Default Heading",$bodytext="Default Body",$t = 10)

$heading = GUICreate($headingtext, 592, 58, 192, 124)
$data = GUICtrlCreateLabel($bodytext, 10, 10, 588, 57)

GUISetIcon(@AutoItExe, -2, $heading)
GUISetState(@SW_SHOW)

$timer = TimerInit()
While 1
	$nMsg = GUIGetMsg()
	Switch $nMsg
		Case $GUI_EVENT_CLOSE
			Exit

	EndSwitch

	if( TimerDiff($timer) >= ($t* 1000) ) then; 5 seconds
       Exit
    EndIf
WEnd
EndFunc

