#include <GUIConstantsEx.au3>
#include <StaticConstants.au3>
#include <WindowsConstants.au3>
#include <Array.au3>

MyMessage("Click Close","YOU MUST HAVE AC POWER ADAPTER PLUGGED IN DURING THIS UPDATE")

Func MyMessage($headingtext="Default Heading",$bodytext="Default Body",$t = 10)

$heading = GUICreate($headingtext, 500, 50, 120, 124)
$data = GUICtrlCreateLabel($bodytext, 10, 10)

GUISetIcon(@AutoItExe, -2, $heading)
Local $idSubmitBotton = GUICtrlCreateButton("Submit", 420, 5, 75, 25)
GUISetState(@SW_SHOW)

$timer = TimerInit()
While 1
	$aMsg = GUIGetMsg(1)
	Select
			Case $aMsg[0] = $GUI_EVENT_CLOSE ; $GUI_EVENT_CLOSE is for close event occured.0 is for Event or Contrl ID.
				GUISwitch($heading)
				GUIDelete()
				ExitLoop
			Case $aMsg[0] = $idSubmitBotton

				GUISwitch($heading)
				GUIDelete()
				ExitLoop
	EndSelect

	if( TimerDiff($timer) >= ($t* 1000) ) then; 5 seconds
       Exit
    EndIf
WEnd
EndFunc