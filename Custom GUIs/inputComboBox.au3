;----------------------------------------------------------------------------------------------------------------------------------------
;Script Name:			Input Combo Box
;Script Version:		1.0
;Purpose:				To create a custom input combo box
;Usage:	inputComboBox	First argument is the first and default value in the combobox and second argument is the "|" separated other values.
;	inputComboBoxArray	Array of values loaded to the combobox and first value is the default.
;Inputs:				2 Arguments(First combobox value,remaining values)	1 Argument(Array of combobox values)
;Outputs:				Selected value in the combobox
;Dependencies:			None
;Author:				Uday Kiran Reddy
;Date:					4th May 2016
;----------------------------------------------------------------------------------------------------------------------------------------

;----------------------------------------------------------------------------------------------------------------------------------------
;Add libraries that we use functions/constants from
;----------------------------------------------------------------------------------------------------------------------------------------

#Include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <GDIPlus.au3>
#include <GuiListView.au3>
#include <GuiComboBox.au3>
#include <GuiEdit.au3>

;MsgBox (0,"",inputComboBox("GPF Shared Services","P1|P2|wscript.exe|cscript.exe"))
;Local $aArray_1D[5] = ["Item 0", "Item 1", "A longer Item 2 to show column expansion", "Item 3", "Item 4"]
;MsgBox (0,"",inputComboBoxArray($aArray_1D))


Func inputComboBox($strFirstValue,$strComboList,$strTitle="Please select the Project")
$windowhandle = GuiCreate($strTitle,300,50)
Local $idInput_SrcPath = GUICtrlCreateCombo($strFirstValue, 15, 15, 150, 25)
GUICtrlSetData($idInput_SrcPath,$strComboList)
Local $idSubmitBotton = GUICtrlCreateButton("Submit", 195, 15, 100, 25)
GuiSetState()

While 1
$aMsg = GUIGetMsg(1)
	Select
			Case $aMsg[0] = $GUI_EVENT_CLOSE ; $GUI_EVENT_CLOSE is for close event occured.0 is for Event or Contrl ID.
				GUISwitch($windowhandle)
				GUIDelete()
				ExitLoop
			Case $aMsg[0] = $idSubmitBotton
				$returnValue = GUICtrlRead($idInput_SrcPath)
				GUISwitch($windowhandle)
				GUIDelete()
				return $returnValue
	EndSelect
WEnd

EndFunc

Func inputComboBoxArray($arrComboList)
	$strFirstValue = $arrComboList[0]
	_ArrayDelete ($arrComboList, 0 )
	$strComboList = _ArrayToString($arrComboList, "|")
	If StringLeft($strComboList, 1) = "|" Then $strComboList = StringRight($strComboList, StringLen($strComboList) - 1)
	If StringRight($strComboList, 1) = "|" Then $strComboList = StringLeft($strComboList, StringLen($strComboList) - 1)
	return inputComboBox($strFirstValue,$strComboList)
EndFunc