You can compile any au3 file by installing autoit from below link.

https://www.autoitscript.com/site/autoit/downloads/

And after installation, right click on au3 file and click on compile and select your required platform and press ok.
The exe will be generated in the same folder.